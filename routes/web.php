<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

$router->post('/posts', function (Request $request) use ($router) {

    try {
        $this->validate($request, [
            'username' => 'required|max:100',
            'comment' => 'required|max:500',
            'email' => 'required|email'
        ]);

        DB::table('posts')->insert(
            [
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'comment' => $request->input('comment'),
                'created_at' => date('Y-m-d H:i:s')
            ]
        );

        return redirect()->route('posts');

    } catch (\Exception $exception) {
        return "Error processing your requests, please use this link to go back to posts<a href='/posts'>Posts</a>";
    }
});

$router->get('/posts', ['as' => 'posts', function (Request $request) use ($router) {

    $posts = DB::table('posts')->get();

    return view('posts', ['posts' => $posts]);
}]);
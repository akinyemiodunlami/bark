

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">

<div class="container">
    <div class="row">
        <div class="col-12">
            <h5>Fill this form to add a post.</h5>
            <form method="post" action="/posts">
                <table class="table table-striped table-bordered table-condensed">
                    <tr>
                        <th>Username</th>
                        <td><input type="text" name="username" value=""/></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><input type="text" name="email" value=""/></td>
                    </tr>
                    <tr>
                        <th>Comment</th>
                        <td>
                            <textarea name="comment">

                            </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input class="btn" type="reset" name="reset" value="Clear Changes">
                            <input class="btn btn-primary" type="submit" name="save-post" value="Save Post">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <h5>Previous Posts.</h5>
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>#.</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Comment</th>
                    <th>Date Posted</th>
                </tr>
                </thead>
                <tbody>
                <?php $counter = 0; ?>
                <?php foreach ($posts as $post) : ?>
                <tr>
                    <td><?= ++$counter; ?></td>
                    <td><?= $post->username; ?></td>
                    <td><?= $post->email; ?></td>
                    <td><?= $post->comment; ?></td>
                    <td><?= $post->created_at; ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

